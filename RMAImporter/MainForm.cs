﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using Aras.IOM;
using System.Collections;

namespace RMAImporter
{
    public partial class MainForm : Form
    {
        private InnovatorConnector mConn;
        private MainForm mMain;
        private List<Item> RMAS;
        private List<Item> UPCSerials;
        private Dictionary<string,string> ProductType;
        private string Team;
        public string database;
        public string server;

        public MainForm()
        {
            this.mConn = new InnovatorConnector();
            this.InitializeComponent();
            
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);

           // this.positionFields();
            
            
            this.txConnection.Text = "Not connected";
            this.mMain = this;
            RMAS = new List<Item>();
            UPCSerials = new List<Item>();
            ProductType = new Dictionary<string, string>();
            Team = "";
        }

        public void setConnection(string connectionString)
        {
            this.txConnection.Text = connectionString;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if ( result == DialogResult.OK )
            {
                try
                {
                    string file = openFileDialog1.FileName;
                    string text = File.ReadAllText(file);
                    ProcessInputFile(text);
                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.Message);
                }
                               
            }
        }

        /*
         Field Order in spreadsheet
         * <customer>                   Item Type - value in record is name string. need to get Customer
         * <agent>                      ITem type - value in record is name string
         * <agent_contact>
         * <agent_contact_name>         String
         * <product_type>               List string
         * <serial_number>              Item Type - Value in record is string. Need to get Serialised Part
         * <item_number>
         * <customer_order_number>      String
         * <site_name>                  String
         * <pump_number>                String
         * <reported_date>              Date string
         * <reason_for_repair>          List string
         * <agent_comment>              String
         * 
         */



        private void ProcessInputFile(string inputFile)
        {
            if (!TryConnect())
            {
                MessageBox.Show("Error connecting to the database. Please try again.");

                return;
            }

            RMAS.Clear();
            try
            {


                Dictionary<string, string> serials = new Dictionary<string, string>();

                ArrayList contents = new ArrayList(inputFile.Split('\n'));
                contents.RemoveAt(0);

                string firstRecord = (string)contents[0];
                string[] firstCells = firstRecord.Split('\t');

                Innovator inn = mConn.getInnovator();
                Item Customer = inn.newItem("Customer", "get");
                Customer.setProperty("name", firstCells[0]);
                Customer.setAttribute("select", "id,team_id");
                Customer = Customer.apply();
                Team = Customer.getProperty("team_id", "");

                Item Agent = inn.newItem("Customer", "get");
                Agent.setProperty("name", firstCells[1]);
                Agent.setAttribute("select", "id");
                Agent = Agent.apply();
                contents.RemoveAt(contents.Count - 1);

                Item AgentContact = inn.newItem("Customer", "get");
                AgentContact.setProperty("contact_name", firstCells[2]);
                AgentContact.setAttribute("select", "id");
                AgentContact = AgentContact.apply();


                foreach (string record in contents)
                {
                    string[] cells = record.Split('\t');
                    if (!ProductType.ContainsKey(cells[3]))
                    {
                        gridErrors.Rows.Add(record, "Product Type " + cells[3] + " is invalid. Record not processed.");
                    }
                    Item SerialNumber = inn.newItem("Part Instance", "get");
                    SerialNumber.setProperty("serial", cells[4]);

                    serials.Add(cells[4], cells[3]);

                    Item RMA = inn.newItem("FT RMA", "add");
                    RMA.setPropertyItem("customer", Customer);
                    RMA.setPropertyItem("agent", Agent);
                    RMA.setPropertyItem("agent_contact", AgentContact);
                    RMA.setProperty("agent_contact_name", cells[2]);
                    RMA.setProperty("team_id", Team);
                    RMA.setProperty("product_type", cells[3]);
                    RMA.setPropertyItem("serial_number", SerialNumber);
                    RMA.setProperty("customer_order_number", cells[6]);
                    RMA.setProperty("site_name", cells[7]);
                    RMA.setProperty("pump_number", cells[6]);
                    RMA.setProperty("reported_date", String.Format("{0:s}", DateTime.Now));
                    RMA.setProperty("reason_for_repair", cells[10]);
                    RMA.setProperty("agent_comment", cells[11]);
                    RMA.setProperty("site_list", cells[12]);
                    RMAS.Add(RMA);

                    gridImportRecords.Rows.Add(cells[0], cells[1], cells[2], cells[3], cells[4], cells[5], cells[6], cells[7], cells[8], cells[9], cells[10], cells[11], cells[12]);


                }

                txNumRecords.Text = RMAS.Count().ToString();

                CheckSerialNumbers(serials);

                backgroundWorker1.RunWorkerAsync();
                //SendToServer();
            }
            catch (System.ArgumentException)
            {
                MessageBox.Show("Duplicate Serial numbers in the RMA import list. Import aborted");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error during import: " + ex.Message + ". Import aborted");
            }

        }

        private void CheckSerialNumbers(Dictionary<string, string> serials)
        {
            List<string> serial = new List<string>(serials.Count);
            foreach (KeyValuePair<string, string> pr in serials)
            {
                serial.Add("('" + pr.Key + "')");
            }
            string allSerials = string.Join(",", serial.ToArray());
            
            string values = "(values " + allSerials + ")";

            string SQL = "Select * From " + values + " as T(ID) Except Select Serial from Part_Instance";

            InnovatorConnector adminConn = new InnovatorConnector();
            adminConn.setConnectionProperties("Admin", database, server, "innovator");
            string connectError = "";
            if ( !adminConn.connect(ref connectError))
            {
                txStatusComments.Text = "Unable to log in as Admin to retrieve list of serial numbers. Missing serial numbers will not be added. Contact Invenco to resolve the admin login issue. " + connectError;
                MessageBox.Show("Unable to log in as Admin to retrieve list of serial numbers. Missing serial numbers will not be added. Contact Invenco to resolve the admin login issue. Import will continue.");
                return;
            }

            Innovator admininn = adminConn.getInnovator();
            Innovator inn = mConn.getInnovator();
            Item serialSearchResult = admininn.applySQL(SQL);


            if (serialSearchResult.getItemCount() > 0)
            {
                List<string> missingSerials = new List<string>(serialSearchResult.getItemCount());
                for (int i = 0; i < serialSearchResult.getItemCount(); i++)
                {
                    Item s = serialSearchResult.getItemByIndex(i);
                    missingSerials.Add(s.getProperty("id"));
                }

                List<string> verifiedUPCSerials = new List<string>();
                List<KeyValuePair<string, string>> invalidSerials = new List<KeyValuePair<string, string>>();
                foreach (string missingSerial in missingSerials)
                {
                    if (serials.ContainsKey(missingSerial))
                    {
                        string serialType = "";
                        serials.TryGetValue(missingSerial, out serialType);
                        if (serialType == "G6 UPC")
                        {
                            Item prt = inn.newItem("Part", "get");
                            prt.setProperty("item_number", "EZ0394");


                            Item upc = inn.newItem("Part Instance", "add");
                            upc.setProperty("serial", missingSerial);
                            upc.setPropertyItem("part", prt);
                            upc.setProperty("manufacturer", "EBB8B811D0364BA782767B3998A5DC55");  //Forth
                            upc.setProperty("mfg_date", String.Format("{0:s}", DateTime.Now));
                            upc.setProperty("remarks", "G6 UPC - Created on import of RMA");
                            upc.setProperty("team_id", Team); 
                            verifiedUPCSerials.Add(upc.node.OuterXml);
                        }
                        else
                        {
                            invalidSerials.Add(new KeyValuePair<string, string>(missingSerial, serialType));
                        }
                    }
                }
                SendMissingSerialsToServer(verifiedUPCSerials);
                // add data to the grid for invalid serial numbers.
                foreach (KeyValuePair<string, string> serialPr in invalidSerials)
                {
                    gridInvalidSerials.Rows.Add(serialPr.Key, serialPr.Value);
                }
            }
            
            
        }


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            backgroundWorker1.ReportProgress(0);
            SendToServer(e);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txStatusComments.Text = "RMA's promted to Submit" + "\r\n" + txStatusComments.Text;
            MessageBox.Show("Import complete");
            
        }


        private void SendMissingSerialsToServer(List<string> upcSerials)
        {
            // send the G6 UPC Serials numbers to the server.
            // this will create all of the G6 UPC Serials that are missing
            
            string AML = "<AML>" + string.Join("",upcSerials.ToArray()) + "</AML>";
            Innovator inn = mConn.getInnovator();
            Item sendResult = inn.applyAML(AML);
            if (sendResult.isError())
            {
                MessageBox.Show("Error sending data to server " + sendResult.getErrorString());
            }
            
       
            
        }

        private void SendToServer(DoWorkEventArgs e)
        {
            // send the RMA data to the server.
            
            // Send the RMAs that need to be created.

            int batchCount = 0;
            double onePercent = 0;

            
            if ( RMAS.Count() > 99)
            {
                onePercent = Math.Round((double)(RMAS.Count() / 100), 0);
            }
            else if (RMAS.Count() > 9)
            {
                onePercent = Math.Round((double)(RMAS.Count() / 10), 0);
            }
            else
            {
                onePercent = 1;
            }
            
            int percentComplete = 0;
            List<Item> successRecords = new List<Item>();
            List<KeyValuePair<string,string>> failRecords = new List<KeyValuePair<string,string>>();
            List<Item> successRecordsCum = new List<Item>();
            List<KeyValuePair<string, string>> failRecordsCum = new List<KeyValuePair<string, string>>();

            List<string> AML = new List<string>();
            //AML.Add("<AML>");
            foreach (Item it in RMAS)
            {
                AML.Add(it.node.OuterXml);
                batchCount++;
                if (batchCount % 20 == 0)
                {
                    SendBatch(ref AML, out successRecords, out failRecords);
                    successRecordsCum.AddRange(successRecords);
                    failRecordsCum.AddRange(failRecords);
                }
                if (batchCount % onePercent == 0)
                {
                    percentComplete++;
                    backgroundWorker1.ReportProgress(percentComplete/2);
                }
            }
            SendBatch(ref AML, out successRecords, out failRecords);
            successRecordsCum.AddRange(successRecords);
            failRecordsCum.AddRange(failRecords);

            ImportResult impRes = new ImportResult(successRecordsCum, failRecordsCum, 2);

            backgroundWorker1.ReportProgress(50,impRes);

            // Promote the new RMA from Raised to Submit
            PromoteToSubmitted(successRecordsCum, onePercent);

            backgroundWorker1.ReportProgress(100);
            e.Result = new ImportResult(successRecordsCum, failRecordsCum,2);
            
        }

        private void PromoteToSubmitted(List<Item> rmas, double onePercent)
        {
            Innovator inn = mConn.getInnovator();
            int batchCount = 0;
            int percentComplete = 50;
            foreach (Item rma in rmas)
            {
                Item serial = inn.newItem("Part Instance", "get");
                serial.setProperty("selectl", rma.getProperty("serial_number"));

                Item installHist = inn.newItem("Part Instance Location History", "get");

                serial.addRelationship(installHist);
                serial = serial.apply();




                if (serial.isError())
                {
                    // the serial number was not found. Should not be able to happen.
                    // just let it go.
                }
                else
                {
                    Item instHists = serial.getItemsByXPath("//Item[@type='Part Instance Location History']");

                    DateTime dt = DateTime.Today;
                    string remDate = String.Format("{0:s}", dt);
                    for (int i = 0; i < instHists.getItemCount(); i++)
                    {
                        Item hist = instHists.getItemByIndex(i);
                        if (hist.getProperty("remove_date", "") == "")
                        {
                            hist.setAction("edit");
                            hist.setProperty("remove_date", remDate);
                            hist = hist.apply();
                        }
                    }
                }
                if (batchCount % onePercent == 0)
                {
                    percentComplete++;
                    backgroundWorker1.ReportProgress(percentComplete);
                }
                ProgramaticVote(rma);
            }
        }

        private void ProgramaticVote(Item rma)
        {
            // Programmatic voting for an activity to facilitate some
            // additional automation

            // called as a generic method.

            // Passed in is the id of the Item that we need to vote for:

            // expecting: <itemId>id</itemId>
            //            <itemType>type</itemType>
            //            <votingPath>path</votingPath>


            //System.Diagnostics.Debugger.Break();

            Innovator inn = mConn.getInnovator();
            

            // retrieve the "Active" workflow activity:

            Item wfcItem = inn.newItem(rma.getType(), "get");  // work flow controlled Item wfcItem
            wfcItem.setID(rma.getID());

            Item workflow = inn.newItem("Workflow", "get");

            Item workflowProcess = inn.newItem("Workflow Process", "get");

            Item wfpa = inn.newItem("Workflow Process Activity", "get");

            Item activity = inn.newItem("Activity", "get");
            activity.setProperty("state", "Active");

            Item activityAssign = inn.newItem("Activity Assignment", "get");

            Item wpp = inn.newItem("Workflow Process Path", "get");
            wpp.setProperty("name", "Submit");

            activity.addRelationship(activityAssign);
            activity.addRelationship(wpp);
            wfpa.setRelatedItem(activity);
            workflowProcess.addRelationship(wfpa);
            workflow.setRelatedItem(workflowProcess);
            wfcItem.addRelationship(workflow);

            wfcItem = wfcItem.apply();

            if (wfcItem.isError())
            {
                //return inn.newError("Error retrieving Workflow Process Item: " + wfcItem.getErrorString());
            }

            Item wfPaths = wfcItem.getItemsByXPath("//Item[@type='Workflow Process Path']");
            if (wfPaths.getItemCount() != 1)
            {
                int x = 50;
                x++;
                //return inn.newError("Unable to get voting path: Submitted");
            }




            //Item wfPaths = this.newItem("Workflow Process Path","get");
            //wfPaths.setProperty("source_id",this.getID());
            //wfPaths.setAttribute("select","id,name");
            //wfPaths = wfPaths.apply();
            //if ( wfPaths.getItemCount() != 2 ){
            //    return inn.newError("Incorrect number of paths from Activity");	
            //}

            string submitPathId = "";

            submitPathId = wfPaths.getItemByIndex(0).getID();


            //for ( int x = 0; x < wfPaths.getItemCount(); x++){
            //    string name = wfPaths.getItemByIndex(x).getProperty("name");
            //    if ( name == "Submit" ){
            //        submitPathId = wfPaths.getItemByIndex(x).getID();
            //    }    
            //}

            Item act = wfcItem.getItemsByXPath("//Item[@type='Workflow Process Activity']/related_id/Item[@type='Activity']");
            if (act.getItemCount() != 1)
            {
                int x = 50;
                x++;
                //return inn.newError("Unable to get activity");
            }

            string actId = act.getID();
            string vote = "Submit";
            string comment = "";
            string assignId = "";


            Item myAlias = inn.newItem("Alias", "get");
            myAlias.setProperty("source_id", inn.getUserID());
            myAlias = myAlias.apply();
            string myIdent = myAlias.getProperty("related_id");

            //System.Diagnostics.Debugger.Break();

            Item actAss = wfcItem.getItemsByXPath("//Item[@type='Activity Assignment']/related_id/Item[@id='" + myIdent + "']");
            if (actAss.getItemCount() != 1)
            {
                int x = 50;
                x++;
                //return inn.newError("Unable to get activity assignment");
            }

            //
            //
            //Item actAss = this.newItem("Activity Assignment","get");
            //actAss.setProperty("source_id",this.getID());
            //actAss.setAttribute("select","id");
            //actAss = actAss.apply();

            //assignId = actAss.getID();
            assignId = actAss.getItemsByXPath("ancestor::Item[@type='Activity Assignment']").getID();
            //assignId = actAss.node.parentNode.parentNode.Attributes["id"].Value;

            //System.Diagnostics.Debugger.Break();

            // Build the voting request
            StringBuilder voteXml = new StringBuilder("");
            voteXml.Append("<Item type=\"Activity\" action=\"EvaluateActivity\">");
            voteXml.Append("  <Activity>{0}</Activity>");
            voteXml.Append("  <ActivityAssignment>{1}</ActivityAssignment>");
            voteXml.Append("  <Paths>");
            voteXml.Append("    <Path id=\"{2}\">{3}</Path>");
            voteXml.Append("  </Paths>");
            voteXml.Append("  <DelegateTo>0</DelegateTo>");
            voteXml.Append("  <Tasks />");
            voteXml.Append("  <Variables />");
            voteXml.Append("  <Authentication mode=\"\" />");
            voteXml.Append("  <Comments>{4}</Comments>");
            voteXml.Append("  <Complete>1</Complete>");
            voteXml.Append("</Item>");

            // Submit the vote
            Item res = inn.newItem();
            res.loadAML(String.Format(voteXml.ToString(), actId, assignId, submitPathId, vote, comment));
            res = res.apply();
            //return res;

        }

        void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // The progress percentage is a property of e
            progressBar1.Value = e.ProgressPercentage;
            if (e.UserState != null)
            {
                ImportResult impRes = (ImportResult)e.UserState;
                if (impRes.state == 2)
                {
                    // Import has completed
                    foreach (Item RMA in impRes.successRecords)
                    {
                        gridCreatedRMAs.Rows.Add(RMA.getProperty("item_number"), RMA.getPropertyAttribute("serial_number", "keyed_name"), RMA.getProperty("product_type"));
                    }
                    foreach (KeyValuePair<string, string> failRecord in impRes.failRecords)
                    {
                        gridErrors.Rows.Add(failRecord.Key, failRecord.Value);
                    }

                    txNumRMA.Text = impRes.successRecords.Count().ToString();
                    txStatusComments.Text = "Import completed. Promoting new RMAs to Submitted State.";
                }

            }
            
        }

        


        private void SendBatch(ref List<string> AML, out List<Item> successRecords, out List<KeyValuePair<string,string>> failRecords)
        {
            Innovator inn = mConn.getInnovator();
            // we have a batch of records
            // send these
            List<Item> lsuccessRecords = new List<Item>();
            List<KeyValuePair<string,string>> lfailRecords = new List<KeyValuePair<string,string>>();
            Item sendResult = inn.applyAML("<AML>" + string.Join("", AML.ToArray()) + "</AML>");
            if (sendResult.isError())
            {
                // try sending one by one to find only the error records.
                foreach(string RMA in AML)
                {
                    Item RMAResult = inn.applyAML("<AML>" + RMA + "</AML>");
                    if ( RMAResult.isError())
                    {
                        lfailRecords.Add(new KeyValuePair<string,string>(RMA,RMAResult.getErrorString()));
                    }
                    else
                    {
                        lsuccessRecords.Add(RMAResult.getItemByIndex(0));
                    }
                }
                //MessageBox.Show("Error sending data to server " + sendResult.getErrorString());
            }
            else
            {
                for ( int i = 0; i < sendResult.getItemCount(); i++)
                {
                    lsuccessRecords.Add(sendResult.getItemByIndex(i));
                }
                
            }
            
            AML.Clear();
            successRecords = lsuccessRecords;
            failRecords = lfailRecords;
        }

        private bool TryConnect()
        {
            if (!mConn.getConnected())
            {
                InnovatorLoginForm lf = new InnovatorLoginForm(ref mConn, this);
                lf.ShowDialog();
                if (!mConn.getConnected())
                {
                    return false;
                }
            }
            return true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //login
            TryConnect();
            if ( mConn.getConnected() )
            {
                // retrieve the List for the Product Type
                Innovator inn = mConn.getInnovator();
                Item productType = inn.newItem("List", "get");
                productType.setProperty("name", "FT Product Type");

                Item values = inn.newItem("Value", "get");
                productType.addRelationship(values);

                productType = productType.apply();

                Item resultsValues = productType.getItemsByXPath("./Relationships/Item");

                for (int i = 0; i < resultsValues.getItemCount(); i++)
                {
                    ProductType.Add(resultsValues.getItemByIndex(i).getProperty("value"),resultsValues.getItemByIndex(i).getProperty("label"));
                }

            }

        }
     
    }

    public class ImportResult
    {
        public List<Item> successRecords;
        public List<KeyValuePair<string,string>> failRecords;
        public int state;

        public ImportResult(List<Item> sRecords, List<KeyValuePair<string,string>> fRecords, int sState)
        {
            successRecords = sRecords;
            failRecords = fRecords;
            state = sState;
        }

    }

    
}
