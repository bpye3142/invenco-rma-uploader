﻿namespace RMAImporter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.txConnection = new System.Windows.Forms.TextBox();
            this.gridInvalidSerials = new System.Windows.Forms.DataGridView();
            this.SerialNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gridCreatedRMAs = new System.Windows.Forms.DataGridView();
            this.RMANumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SerialNumber2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.txNumRecords = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txNumRMA = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.gridErrors = new System.Windows.Forms.DataGridView();
            this.Input = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Error = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.txStatusComments = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.gridImportRecords = new System.Windows.Forms.DataGridView();
            this.Customer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Agent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ContactName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductType1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SerialNumber1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TemNumber1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SiteName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pump = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SiteReportDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ResonForRepair = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgentComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SiteList = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridInvalidSerials)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCreatedRMAs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridErrors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridImportRecords)).BeginInit();
            this.SuspendLayout();
            // 
            // txConnection
            // 
            this.txConnection.Location = new System.Drawing.Point(12, 628);
            this.txConnection.Name = "txConnection";
            this.txConnection.Size = new System.Drawing.Size(553, 20);
            this.txConnection.TabIndex = 0;
            // 
            // gridInvalidSerials
            // 
            this.gridInvalidSerials.AllowUserToAddRows = false;
            this.gridInvalidSerials.AllowUserToDeleteRows = false;
            this.gridInvalidSerials.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridInvalidSerials.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SerialNumber,
            this.Type});
            this.gridInvalidSerials.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.gridInvalidSerials.Location = new System.Drawing.Point(12, 245);
            this.gridInvalidSerials.Name = "gridInvalidSerials";
            this.gridInvalidSerials.ReadOnly = true;
            this.gridInvalidSerials.Size = new System.Drawing.Size(324, 179);
            this.gridInvalidSerials.TabIndex = 2;
            // 
            // SerialNumber
            // 
            this.SerialNumber.HeaderText = "Serial Number";
            this.SerialNumber.Name = "SerialNumber";
            this.SerialNumber.ReadOnly = true;
            this.SerialNumber.Width = 130;
            // 
            // Type
            // 
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            this.Type.Width = 130;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(134, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 31);
            this.button1.TabIndex = 3;
            this.button1.Text = "Select Import File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(16, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 32);
            this.button2.TabIndex = 4;
            this.button2.Text = "Login";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 229);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Invalid Serial Numbers";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 427);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Errors";
            // 
            // gridCreatedRMAs
            // 
            this.gridCreatedRMAs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridCreatedRMAs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RMANumber,
            this.SerialNumber2,
            this.Type2});
            this.gridCreatedRMAs.Location = new System.Drawing.Point(364, 245);
            this.gridCreatedRMAs.Name = "gridCreatedRMAs";
            this.gridCreatedRMAs.Size = new System.Drawing.Size(366, 179);
            this.gridCreatedRMAs.TabIndex = 8;
            // 
            // RMANumber
            // 
            this.RMANumber.HeaderText = "RMA Number";
            this.RMANumber.Name = "RMANumber";
            // 
            // SerialNumber2
            // 
            this.SerialNumber2.HeaderText = "Serial Number";
            this.SerialNumber2.Name = "SerialNumber2";
            // 
            // Type2
            // 
            this.Type2.HeaderText = "Type";
            this.Type2.Name = "Type2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(361, 229);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "RMA Created";
            // 
            // txNumRecords
            // 
            this.txNumRecords.Location = new System.Drawing.Point(953, 245);
            this.txNumRecords.Name = "txNumRecords";
            this.txNumRecords.ReadOnly = true;
            this.txNumRecords.Size = new System.Drawing.Size(100, 20);
            this.txNumRecords.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(748, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Number of Import Records";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(748, 289);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Number of RMA Created";
            // 
            // txNumRMA
            // 
            this.txNumRMA.Location = new System.Drawing.Point(953, 282);
            this.txNumRMA.Name = "txNumRMA";
            this.txNumRMA.ReadOnly = true;
            this.txNumRMA.Size = new System.Drawing.Size(100, 20);
            this.txNumRMA.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 612);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Database Connection";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(583, 629);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(483, 19);
            this.progressBar1.TabIndex = 16;
            // 
            // gridErrors
            // 
            this.gridErrors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridErrors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Input,
            this.Error});
            this.gridErrors.Location = new System.Drawing.Point(18, 443);
            this.gridErrors.Name = "gridErrors";
            this.gridErrors.Size = new System.Drawing.Size(1048, 145);
            this.gridErrors.TabIndex = 17;
            // 
            // Input
            // 
            this.Input.HeaderText = "Input";
            this.Input.Name = "Input";
            this.Input.Width = 500;
            // 
            // Error
            // 
            this.Error.HeaderText = "Error";
            this.Error.Name = "Error";
            this.Error.Width = 500;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(583, 610);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "RMA Import Progress";
            // 
            // txStatusComments
            // 
            this.txStatusComments.Location = new System.Drawing.Point(751, 360);
            this.txStatusComments.Multiline = true;
            this.txStatusComments.Name = "txStatusComments";
            this.txStatusComments.Size = new System.Drawing.Size(301, 64);
            this.txStatusComments.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(748, 331);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "Import Process Comments";
            // 
            // gridImportRecords
            // 
            this.gridImportRecords.AllowUserToAddRows = false;
            this.gridImportRecords.AllowUserToDeleteRows = false;
            this.gridImportRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridImportRecords.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Customer,
            this.Agent,
            this.ContactName,
            this.ProductType1,
            this.SerialNumber1,
            this.TemNumber1,
            this.OrderNumber,
            this.SiteName,
            this.Pump,
            this.SiteReportDate,
            this.ResonForRepair,
            this.AgentComment,
            this.SiteList});
            this.gridImportRecords.Location = new System.Drawing.Point(12, 66);
            this.gridImportRecords.Name = "gridImportRecords";
            this.gridImportRecords.ReadOnly = true;
            this.gridImportRecords.Size = new System.Drawing.Size(1054, 150);
            this.gridImportRecords.TabIndex = 21;
            // 
            // Customer
            // 
            this.Customer.HeaderText = "Customer";
            this.Customer.Name = "Customer";
            this.Customer.ReadOnly = true;
            // 
            // Agent
            // 
            this.Agent.HeaderText = "Agent";
            this.Agent.Name = "Agent";
            this.Agent.ReadOnly = true;
            // 
            // ContactName
            // 
            this.ContactName.HeaderText = "Contact Name";
            this.ContactName.Name = "ContactName";
            this.ContactName.ReadOnly = true;
            // 
            // ProductType1
            // 
            this.ProductType1.HeaderText = "Product Type";
            this.ProductType1.Name = "ProductType1";
            this.ProductType1.ReadOnly = true;
            // 
            // SerialNumber1
            // 
            this.SerialNumber1.HeaderText = "Serial";
            this.SerialNumber1.Name = "SerialNumber1";
            this.SerialNumber1.ReadOnly = true;
            // 
            // TemNumber1
            // 
            this.TemNumber1.HeaderText = "Item Number";
            this.TemNumber1.Name = "TemNumber1";
            this.TemNumber1.ReadOnly = true;
            // 
            // OrderNumber
            // 
            this.OrderNumber.HeaderText = "Order Number";
            this.OrderNumber.Name = "OrderNumber";
            this.OrderNumber.ReadOnly = true;
            // 
            // SiteName
            // 
            this.SiteName.HeaderText = "SiteName";
            this.SiteName.Name = "SiteName";
            this.SiteName.ReadOnly = true;
            // 
            // Pump
            // 
            this.Pump.HeaderText = "Pump";
            this.Pump.Name = "Pump";
            this.Pump.ReadOnly = true;
            // 
            // SiteReportDate
            // 
            this.SiteReportDate.HeaderText = "Site Report Date";
            this.SiteReportDate.Name = "SiteReportDate";
            this.SiteReportDate.ReadOnly = true;
            // 
            // ResonForRepair
            // 
            this.ResonForRepair.HeaderText = "Reason For Repair";
            this.ResonForRepair.Name = "ResonForRepair";
            this.ResonForRepair.ReadOnly = true;
            // 
            // AgentComment
            // 
            this.AgentComment.HeaderText = "Agent Comment";
            this.AgentComment.Name = "AgentComment";
            this.AgentComment.ReadOnly = true;
            // 
            // SiteList
            // 
            this.SiteList.HeaderText = "Site List";
            this.SiteList.Name = "SiteList";
            this.SiteList.ReadOnly = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 699);
            this.Controls.Add(this.gridImportRecords);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txStatusComments);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.gridErrors);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txNumRMA);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txNumRecords);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gridCreatedRMAs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gridInvalidSerials);
            this.Controls.Add(this.txConnection);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "RMA Importer";
            this.Click += new System.EventHandler(this.btnClose_Click);
            ((System.ComponentModel.ISupportInitialize)(this.gridInvalidSerials)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCreatedRMAs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridErrors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridImportRecords)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txConnection;
        private System.Windows.Forms.DataGridView gridInvalidSerials;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView gridCreatedRMAs;
        private System.Windows.Forms.DataGridViewTextBoxColumn RMANumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNumber2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txNumRecords;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txNumRMA;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataGridView gridErrors;
        private System.Windows.Forms.DataGridViewTextBoxColumn Input;
        private System.Windows.Forms.DataGridViewTextBoxColumn Error;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txStatusComments;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView gridImportRecords;
        private System.Windows.Forms.DataGridViewTextBoxColumn Customer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Agent;
        private System.Windows.Forms.DataGridViewTextBoxColumn ContactName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductType1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNumber1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TemNumber1;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn SiteName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pump;
        private System.Windows.Forms.DataGridViewTextBoxColumn SiteReportDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ResonForRepair;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgentComment;
        private System.Windows.Forms.DataGridViewTextBoxColumn SiteList;
    }
}