﻿namespace RMAImporter
{
    partial class InnovatorLoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txUser = new System.Windows.Forms.TextBox();
            this.txPassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txServer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txStatus = new System.Windows.Forms.TextBox();
            this.cboDatabases = new System.Windows.Forms.ComboBox();
            this.btnGetDatabases = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(13, 240);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(133, 29);
            this.btnLogin.TabIndex = 0;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click_1);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(407, 242);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(139, 27);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txUser
            // 
            this.txUser.Location = new System.Drawing.Point(138, 37);
            this.txUser.Name = "txUser";
            this.txUser.Size = new System.Drawing.Size(341, 20);
            this.txUser.TabIndex = 2;
            // 
            // txPassword
            // 
            this.txPassword.Location = new System.Drawing.Point(138, 63);
            this.txPassword.Name = "txPassword";
            this.txPassword.Size = new System.Drawing.Size(341, 20);
            this.txPassword.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "User";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Password";
            // 
            // txServer
            // 
            this.txServer.Location = new System.Drawing.Point(137, 89);
            this.txServer.Name = "txServer";
            this.txServer.Size = new System.Drawing.Size(342, 20);
            this.txServer.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Server";
            // 
            // txStatus
            // 
            this.txStatus.Location = new System.Drawing.Point(13, 143);
            this.txStatus.Multiline = true;
            this.txStatus.Name = "txStatus";
            this.txStatus.Size = new System.Drawing.Size(533, 86);
            this.txStatus.TabIndex = 8;
            // 
            // cboDatabases
            // 
            this.cboDatabases.FormattingEnabled = true;
            this.cboDatabases.Location = new System.Drawing.Point(137, 115);
            this.cboDatabases.Name = "cboDatabases";
            this.cboDatabases.Size = new System.Drawing.Size(342, 21);
            this.cboDatabases.TabIndex = 9;
            // 
            // btnGetDatabases
            // 
            this.btnGetDatabases.Location = new System.Drawing.Point(485, 116);
            this.btnGetDatabases.Name = "btnGetDatabases";
            this.btnGetDatabases.Size = new System.Drawing.Size(24, 20);
            this.btnGetDatabases.TabIndex = 10;
            this.btnGetDatabases.Text = "...";
            this.btnGetDatabases.UseVisualStyleBackColor = true;
            this.btnGetDatabases.Click += new System.EventHandler(this.btnGetDatabases_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Databases";
            // 
            // InnovatorLoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 279);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnGetDatabases);
            this.Controls.Add(this.cboDatabases);
            this.Controls.Add(this.txStatus);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txServer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txPassword);
            this.Controls.Add(this.txUser);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnLogin);
            this.Name = "InnovatorLoginForm";
            this.Text = "InnovatorLoginForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txUser;
        private System.Windows.Forms.TextBox txPassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txServer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txStatus;
        private System.Windows.Forms.ComboBox cboDatabases;
        private System.Windows.Forms.Button btnGetDatabases;
        private System.Windows.Forms.Label label4;
    }
}