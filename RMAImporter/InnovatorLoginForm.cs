﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Xml;

namespace RMAImporter
{
    public partial class InnovatorLoginForm : Form
    {
        private InnovatorConnector myInnConn;
        private string cookiePath;
        private string loginCredentialsPath;
        private MainForm mMainBackPointer;

         public InnovatorLoginForm(ref InnovatorConnector innConn, MainForm main)
        {
      
          this.mMainBackPointer = main;
          this.InitializeComponent();
          this.myInnConn = innConn;
          this.cookiePath = Environment.GetFolderPath(Environment.SpecialFolder.Cookies);
          this.loginCredentialsPath = this.cookiePath + "\\arasLogin.txt";
          try
          {
            StreamReader streamReader = new StreamReader(this.loginCredentialsPath);
            this.txUser.Text = streamReader.ReadLine();
            this.txServer.Text = streamReader.ReadLine();
            this.cboDatabases.Text = streamReader.ReadLine();
            streamReader.Close();
            this.btnGetDatabases_Click((object) this, (EventArgs) null);
          }
          catch (Exception ex)
          {
              
          }
        }

         private void btnClose_Click(object sender, EventArgs e)
         {
             this.Close();
         }

         private void btnLogin_Click_1(object sender, EventArgs e)
         {
             this.txStatus.AppendText("Initiating Connection\n");
             string text1 = this.txUser.Text;
             string text2 = this.txPassword.Text;
             string text3 = this.txServer.Text;
             string text4 = this.cboDatabases.Text;
             this.mMainBackPointer.database = text4;
             this.mMainBackPointer.server = text3;
             string errorText = "";
             StreamWriter streamWriter = new StreamWriter(this.loginCredentialsPath, false);
             streamWriter.WriteLine(text1);
             streamWriter.WriteLine(text3);
             streamWriter.WriteLine(text4);
             streamWriter.Close();
             this.myInnConn.setConnectionProperties(text1, text4, text3, text2);
             if (this.myInnConn.connect(ref errorText))
             {
                 this.txStatus.AppendText("Connection Established to : " + text3 + "/" + text4 + "\n");
                 this.mMainBackPointer.setConnection(text3 + "/" + text4);
                 
                 this.Close();
             }
             else
             {
                 this.txStatus.AppendText("Error connecting to database :" + errorText + "\n");
                 
             }
         }

         private void btnGetDatabases_Click(object sender, EventArgs e)
         {
             this.cboDatabases.Items.Clear();
             try
             {
                 HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(this.txServer.Text + "/Server/DBList.aspx");
                 httpWebRequest.Method = "GET";
                 HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                 XmlDocument xmlDocument = new XmlDocument();
                 xmlDocument.Load(httpWebResponse.GetResponseStream());
                 foreach (XmlNode xmlNode in xmlDocument.SelectNodes("/DBList/DB"))
                     this.cboDatabases.Items.Add((object)xmlNode.Attributes[0].Value);
             }
             catch (Exception ex)
             {
                 if (ex.Message.IndexOf("404") > 0)
                 {
                     int num1 = (int)MessageBox.Show("The entered server name is incorrect. Please try again");
                 }
                 else
                 {
                     int num2 = (int)MessageBox.Show("Error retrieving database names : " + ex.Message);
                 }
             }
         }

        
    }
}
