﻿// Type: InnovatorModifiedTemplates.InnovatorConnector
// Assembly: InnovatorModifiedTemplates, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: D:\Visual Studio Projects\Projects\InnovatorModifiedTemplates\InnovatorModifiedTemplates\bin\Release\InnovatorModifiedTemplates.exe

using Aras.IOM;
using System;
using System.Security.Cryptography;
using System.Text;

namespace RMAImporter
{
  public class InnovatorConnector
  {
    private string m_user;
    private string m_database;
    private string m_server;
    private string m_password;
    private HttpServerConnection m_connection;
    private Innovator m_innovator;
    private bool m_connectionPropsSet;
    private bool m_connected;

    public InnovatorConnector()
    {
     
      this.m_password = "";
      this.m_user = "";
      this.m_database = "";
      this.m_server = "";
      this.m_connectionPropsSet = false;
      this.m_connected = false;
    }

    public InnovatorConnector(string user, string database, string server, string plainPassword)
    {
      
      this.m_password = this.convertPasswordToMD5(plainPassword);
      this.m_user = user;
      this.m_database = database;
      this.m_server = server;
      this.m_connectionPropsSet = true;
      this.m_connected = false;
    }

    ~InnovatorConnector()
    {
      try
      {
      }
      finally
      {
        
      }
    }

    public void setConnectionProperties(string user, string database, string server, string plainPassword)
    {
      this.m_password = this.convertPasswordToMD5(plainPassword);
      this.m_user = user;
      this.m_database = database;
      this.m_server = server;
      this.m_connectionPropsSet = true;
    }

    public string getServerName()
    {
      return this.m_server;
    }

    public HttpServerConnection getConnection()
    {
      return this.m_connection;
    }

    public bool getConnected()
    {
      return this.m_connected;
    }

    public Innovator getInnovator()
    {
      return this.m_innovator;
    }

    public bool connect(ref string errorText)
    {
      if (!this.m_connectionPropsSet)
      {
        errorText = "Connection properties not set. Call setConnectionProperties before calling connect";
        return false;
      }
      else
      {
        Item obj;
        try
        {
          this.m_connection = IomFactory.CreateHttpServerConnection(this.m_server + "/Server/InnovatorServer.aspx", this.m_database, this.m_user, this.m_password);
          obj = this.m_connection.Login();
        }
        catch (Exception ex)
        {
          errorText = ex.Message;
          return false;
        }
        if (obj.isError())
        {
          this.m_innovator = (Innovator) null;
          errorText = obj.getErrorString();
          return false;
        }
        else
        {
          this.m_innovator = new Innovator((IServerConnection) this.m_connection);
          this.m_connected = true;
          return true;
        }
      }
    }

    public bool disconnect(ref HttpServerConnection myConnection, ref string errorText)
    {
      try
      {
        myConnection.Logout();
        this.m_connected = false;
        return true;
      }
      catch (Exception ex)
      {
        errorText = ex.Message;
        return false;
      }
    }

    public string convertPasswordToMD5(string plainPassword)
    {
      return this.binaryToHex(new MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(plainPassword))).ToLower();
    }

    private string binaryToHex(byte[] binaryArray)
    {
      string str = "";
      for (int index = 0; index < binaryArray.Length; ++index)
      {
        byte num1 = binaryArray[index];
        long num2 = (long) ((int) num1 & 15);
        long num3 = (long) ((int) num1 >> 4);
        str = str + (object) this.numberToHex(num3) + (object) this.numberToHex(num2);
      }
      return str;
    }

    private char numberToHex(long num)
    {
      if (num > 9L)
        return Convert.ToChar(65L + (num - 10L));
      else
        return Convert.ToChar(48L + num);
    }
  }
}
